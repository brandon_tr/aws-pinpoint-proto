package tr.demo.pinpoint.model;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
public enum EventType {
      SendCampaign("_campaign.send"),
      Purchase("_monetization.purchase"),
      StartSession("_session.start"),
      StopSession("_session.stop"),
      PauseSession("_session.pause"),
      ResumeSession("_session.resume"),
      UserSignIn("_userauth.sign_in"),
      UserSignUp("_userauth.sign_up"),
      UserAuthFail("_userauth.auth_fail"),
      SendEmail("_email.send"),
      EmailDelivered("_email.delivered"),
      EmailRejected("_email.rejected"),
      EmailHardBounce("_email.hardbounce"),
      EmailSoftBounce("_email.softbounce"),
      EmailComplaint("_email.complaint"),
      EmailOpen("_email.open"),
      EmailClick("_email.click"),
      EmailUnsubscribe("_email.unsubscribe"),
      SmsSend("_sms.send"),
      SmsSuccess("_sms.success"),
      SmsFail("_sms.fail"),
      SmsOptOut("_sms.optout");

      @Getter
      final String name;
}
