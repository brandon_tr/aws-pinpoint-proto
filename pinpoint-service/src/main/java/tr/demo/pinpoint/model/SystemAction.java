package tr.demo.pinpoint.model;

import java.util.Map;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SystemAction {
  final String traceId;
  final Referrer referrer;
  final EventType eventType;
  final String timestamp;
  final Map<String, Double> metrics;
  final Map<String, String> attributes;
}
