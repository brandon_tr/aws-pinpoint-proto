package tr.demo.pinpoint.model;

public enum Referrer {
  Facebook,
  Indeed,
  ZipRecruiter
}
