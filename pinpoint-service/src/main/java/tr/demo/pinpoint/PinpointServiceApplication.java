package tr.demo.pinpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("tr.demo.pinpoint.*")
public class PinpointServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PinpointServiceApplication.class, args);
	}
}
