package tr.demo.pinpoint.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.pinpoint.PinpointClient;

@Configuration
public class PinpointClientConfiguration {

  @Bean
  @ConfigurationProperties("aws")
  public DefaultCredentialsProvider awsConfig() {
    return DefaultCredentialsProvider.builder().build();
  }

  @Bean
  public PinpointClient pinpointClient(DefaultCredentialsProvider defaultCredentialsProvider) {
    return PinpointClient.builder()
        .region(Region.US_WEST_2)
        .credentialsProvider(defaultCredentialsProvider)
        .build();
  }
}
