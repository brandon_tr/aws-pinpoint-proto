package tr.demo.pinpoint.service;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.pinpoint.PinpointClient;
import software.amazon.awssdk.services.pinpoint.model.ChannelType;
import software.amazon.awssdk.services.pinpoint.model.EndpointDemographic;
import software.amazon.awssdk.services.pinpoint.model.EndpointRequest;
import software.amazon.awssdk.services.pinpoint.model.EndpointUser;
import software.amazon.awssdk.services.pinpoint.model.Event;
import software.amazon.awssdk.services.pinpoint.model.EventsBatch;
import software.amazon.awssdk.services.pinpoint.model.EventsRequest;
import software.amazon.awssdk.services.pinpoint.model.GetApplicationDateRangeKpiRequest;
import software.amazon.awssdk.services.pinpoint.model.GetAppsRequest;
import software.amazon.awssdk.services.pinpoint.model.GetEndpointRequest;
import software.amazon.awssdk.services.pinpoint.model.PublicEndpoint;
import software.amazon.awssdk.services.pinpoint.model.PutEventsRequest;
import software.amazon.awssdk.services.pinpoint.model.UpdateEndpointRequest;
import tr.demo.pinpoint.model.SystemAction;

@Service
@Slf4j
@AllArgsConstructor
public class AwsPinpoint {

  private final ApplicationContext applicationContext;
  private final PinpointClient client;

  public String getEndpoint(final String applicationId, final String endpointId) {
    log.info("get endpoint with applicationId {} and endpointId {}", applicationId, endpointId);
    return client.getEndpoint(GetEndpointRequest.builder()
        .applicationId(applicationId)
        .endpointId(endpointId)
        .build())
        .toString();
  }

  public String updateEndpoint(final String applicationId, final String endpointId) {
    log.info("update endpointId {} for applicationId {}", endpointId, applicationId);
    return client.updateEndpoint(UpdateEndpointRequest.builder()
        .applicationId(applicationId)
        .endpointId(endpointId)
        .endpointRequest(EndpointRequest.builder()
            .address(endpointId)
            .channelType(ChannelType.CUSTOM)
            .user(EndpointUser.builder().userId(applicationContext.getApplicationName()).build())
            .metrics(Map.of("endpointUpdate", 1.0))
            .build())
        .build())
        .toString();
  }

  public String putEvents(final String applicationId, final SystemAction event) {
    var startTimestamp = ZonedDateTime
        .now(ZoneOffset.UTC)
        .format(DateTimeFormatter.ISO_INSTANT);

    log.info("put events {} for {}", event, event.getTraceId());
    return client.putEvents(PutEventsRequest.builder()
        .applicationId(applicationId)
        .eventsRequest(EventsRequest.builder()
            .batchItem(Map.of(event.getTraceId(), EventsBatch.builder()
                .endpoint(PublicEndpoint.builder()
                    .user(EndpointUser.builder()
                        .userId("prototype-user")
                        .build())
                    .build())
                .endpoint(PublicEndpoint.builder()
                    .demographic(EndpointDemographic.builder()
                        .appVersion("api")
                        .build())
                    .build())
                .events(Map.of(event.getTraceId(), Event.builder()
                    .appTitle(event.getReferrer().name())
                    .timestamp(startTimestamp)
                    .eventType(event.getEventType().name())
                    .attributes(event.getAttributes())
                    .metrics(event.getMetrics())
                    .build()))
                .build()))
            .build())
        .build())
        .toString();
  }

  public String getApplicationDateRangeKpi(final String applicationId, final String kpi) {
    var response = client.getApps(GetAppsRequest.builder().build());

    log.info("get kpi {} for application id {}", kpi, applicationId);
    return client
        .getApplicationDateRangeKpi(GetApplicationDateRangeKpiRequest
            .builder()
            .applicationId(applicationId)
            .kpiName(kpi)
            .startTime(Instant.now().minus(Duration.ofDays(15)))
            .endTime(Instant.now())
            .build())
        .toString();
  }
}
