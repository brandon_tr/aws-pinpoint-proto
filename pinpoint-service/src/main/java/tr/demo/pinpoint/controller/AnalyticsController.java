package tr.demo.pinpoint.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tr.demo.pinpoint.model.SystemAction;
import tr.demo.pinpoint.service.AwsPinpoint;

@AllArgsConstructor
@RestController
public class AnalyticsController {

  private final AwsPinpoint awsPinpoint;

  @GetMapping("/analytics/{applicationId}/{kpi}")
  public ResponseEntity<String> getApplicationDateRangeKpi(
      @PathVariable("applicationId") String applicationId,
      @PathVariable("kpi") String kpi) {
    return ResponseEntity.ok(awsPinpoint.getApplicationDateRangeKpi(applicationId, kpi));
  }

  @PostMapping("/analytics/{applicationId}/event")
  public ResponseEntity<String> putEvents(
      @PathVariable("applicationId") String applicationId,
      @RequestBody SystemAction event) {
    return ResponseEntity.ok(awsPinpoint.putEvents(applicationId, event));
  }

  @GetMapping("/analytics/{applicationId}/endpoint/{endpointId}")
  public ResponseEntity<String> getEndpoint(
      @PathVariable("applicationId") final String applicationId,
      @PathVariable("endpointId") final String endpointId) {
    return ResponseEntity.ok(awsPinpoint.getEndpoint(applicationId, endpointId));
  }

  @PutMapping("/analytics/{applicationId}/endpoint/{endpointId}")
  public ResponseEntity<String> updateEndpoint(
      @PathVariable("applicationId") String applicationId,
      @PathVariable String endpointId) {
    return ResponseEntity.ok(awsPinpoint.updateEndpoint(applicationId, endpointId));
  }
}
