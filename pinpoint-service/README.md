### config

```shell script
export ACTUATOR_EXPOSED_ENDPOINTS=info,health,env,httptrace,metrics
* export AWS_STACK_NAME=[!!AWS_STACK_NAME!!]
** export AWS_ACCESS_KEY=[!!AWS_ACCESS_KEY!!]
** export AWS_SECRET_KEY=[!!AWS_SECRET_KEY!!]
```

* stack name referenced in cloudformation
** access & secret key should be from your iam user/env service acct


