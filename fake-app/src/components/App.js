import React from 'react'
import './App.css'
import Grid from '@material-ui/core/Grid'
import {Button} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import styled from 'styled-components'
import pinpoint from '../services/pinpoint'
import {Analytics} from '@aws-amplify/analytics'

const DefaultButton = styled(Button)`{
  width: 75%;
  margin-bottom: .5rem;
}`

const Header = styled(Typography)`{
  width: 100%;
  text-align: center;
}`

function App() {
  pinpoint.autoTrackSession()
  pinpoint.autoTrackPageView()

  const standardAppEvents = [
    '_campaign.send',
    '_monetization.purchase',
    '_session.start',
    '_session.stop',
    '_session.pause',
    '_session.resume',
    '_userauth.sign_in',
    '_userauth.sign_up',
    '_userauth.auth_fail']

  const standardEmailEvents = [
    '_email.send',
    '_email.delivered',
    '_email.rejected',
    '_email.hardbounce',
    '_email.softbounce',
    '_email.complaint',
    '_email.open',
    '_email.click',
    '_email.unsubscribe']

  const standardSmsEvents = [
    '_sms.send',
    '_sms.success',
    '_sms.fail',
    '_sms.optout']

  const buttonsFrom = (events) => events.map((event, idx) => {
    const color = idx % 2 === 0 ? 'primary' : 'secondary'
    return <DefaultButton variant="contained"
                          key={event}
                          color={color}
                          onClick={() => Analytics.record(event)
                              .then(r => console.info(r, event))}>
      {event}
    </DefaultButton>
  })

  return <Grid container
               spacing={0}
               direction="column"
               alignItems="center"
               justify="center"
               style={{minHeight: '100vh'}}>

    <Header variant="h1" gutterBottom>
      fake app
    </Header>

    <Grid container
          direction="row"
          justify="center"
          alignItems="center">

      <Header variant="h4">
        standard app events
      </Header>
      {buttonsFrom(standardAppEvents)}

      <Header variant="h4">
        standard email events
      </Header>
      {buttonsFrom(standardEmailEvents)}

      <Header variant="h4">
        standard sms events
      </Header>
      {buttonsFrom(standardSmsEvents)}
    </Grid>
  </Grid>
}

export default App
