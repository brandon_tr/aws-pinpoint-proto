import {Analytics} from '@aws-amplify/analytics'

const autoTrackSession = () => {
  Analytics.autoTrack('session', {
    enable: true,
  })
}

const autoTrackPageView = () => {
  Analytics.autoTrack('pageView', {
    enable: true,
    eventName: 'pageView',
    attributes: {
      jobDescription: 1234,
    },
    type: 'SPA',
    provider: 'AWSPinpoint',
  })
}

const pinpoint = {
  autoTrackSession,
  autoTrackPageView
}

export default pinpoint
