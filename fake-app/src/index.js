import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './components/App'
import reportWebVitals from './reportWebVitals'
import {Amplify} from '@aws-amplify/core'
import {Auth} from '@aws-amplify/auth'
import {Analytics} from '@aws-amplify/analytics'

// window.LOG_LEVEL='DEBUG'
Amplify.configure(JSON.parse(process.env.REACT_APP_AWS_CONFIG))
Auth.configure(JSON.parse(process.env.REACT_APP_AWS_CONFIG))

ReactDOM.render(
    <React.StrictMode>
      <App/>
    </React.StrictMode>,
    document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals((metric) => {
  Analytics.record(metric)
      .then(r => console.info(r))
      .catch(r => console.error(r))
})
