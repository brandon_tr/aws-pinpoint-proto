/* eslint-disable */

const awsConfiguration = JSON.parse(process.env.REACT_APP_AWS_CONFIG)
const awsmobile = {
    "aws_project_region": awsConfiguration["aws_project_region"],
    "aws_cognito_identity_pool_id": awsConfiguration["aws_cognito_identity_pool_id"],
    "aws_cognito_region": awsConfiguration["aws_cognito_region"],
    "oauth": awsConfiguration["oauth"],
    "aws_mobile_analytics_app_id": awsConfiguration["aws_mobile_analytics_app_id"],
    "aws_mobile_analytics_app_region": awsConfiguration["aws_mobile_analytics_app_region"],
    "aws_content_delivery_bucket": awsConfiguration["aws_content_delivery_bucket"],
    "aws_content_delivery_bucket_region": awsConfiguration["aws_content_delivery_bucket_region"],
    "aws_content_delivery_url": awsConfiguration["aws_content_delivery_url"],
};


export default awsmobile;
